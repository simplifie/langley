<?php
namespace simplifie;
use OAuth2\Autoloader as Autoloader;
Autoloader::register();
use OAuth2\Response as Response;
use OAuth2\Request as Request;
use OAuth2\GrantType\ClientCredentials as ClientCredentials;
use OAuth2\GrantType\AuthorizationCode as AuthorizationCode;
use OAuth2\GrantType\RefreshToken as RefreshToken;
use OAuth2\GrantType\UserCredentials as UserCredentials;
use OAuth2\Server as Server;
use OAuth2\Storage\Pdo as Pdo;
use OAuth2\Storage\Memory as Memory;
use OAuth2\Scope as Scope;
class OAuth2Server extends Server
{
  private $aValidScopes;
  public function __construct(
    $db = 
    [
      'engine'   => 'mysql', 
      'name'     => 'langley', 
      'host'     => 'localhost', 
      'username' => 'root', 
      'password' => ''
    ],
    $allowImplicit = false
  )
  {
    $s = "{$db['engine']}:dbname={$db['name']};host={$db['host']}";
    $storage = new Pdo(
      [
        'dsn'      => $s, 
        'username' => $db['username'], 
        'password' => $db['password']
      ]
    );
    parent::__construct(
      $storage, 
      ['allow_implicit' => $allowImplicit]
    );
    $this->addGrantType(new ClientCredentials($storage));
    $this->addGrantType(new AuthorizationCode($storage));
    $this->addGrantType(new RefreshToken($storage));
    $this->addGrantType(new UserCredentials($storage));
    $this->setValidScopes();
  }
  public final function setValidScopes($validScopes = null)
  {
    if(empty($validScopes))
    {
      $default = 'user_read';
      $this->aValidScopes = 
      [
        'default' => $default,
        'supported' =>
        [
          'user_create',
          $default,
          'user_update',
          'user_delete'
        ]
      ];
    }
    else
    {
      $this->aValidScopes = $validScopes;
    }
    $m = new Memory
    (
      [
        'default_scope'    => $default,
        'supported_scopes' => $this->getValidScopes()['supported']
      ]
    );
    $this->setScopeUtil(new Scope($m));
  }
  public final function getValidScopes()
  {
    return $this->aValidScopes;
  }
  /**
   * Requires the user to authenticate and redirects 
   * back to the client with an Authorization Code Grant Type.
   * Implicit Grant Type is currently not supported.
   * @access  public
   * @see  http://www.oauth2.com/granttypes
   * @return string The code to authorize.
   */
  /*
    To show a UI, issue a GET request using 
    curl "/authorize&response_type=code&client_id=CLIENT_ID&state=XYZ"
    
    To get an authorization code, issue a request using 
    curl -d "authorized=yes" \
         "/authorize?response_type=code&client_id=testclient&state=xyz"
  */
  public final function authorize($authorized = false)
  {
    $request = Request::createFromGlobals();
    $response = new Response();
    if(!$this->validateAuthorizeRequest($request, $response)) 
    {
      $response->send();
      exit;
    }
    //
    $this->handleAuthorizeRequest($request, $response, $authorized);
    if($authorized) 
    {
      $l = $response->getHttpHeader('Location');
      $code = substr($l, strpos($l, 'code=') + 5, 40);
      return $code;
    }
    //
    $response->send();
  }
  /**
   * For any resource request (i.e. API Call) requiring 
   * OAuth2 operation. This validates the incoming 
   * request and serves back the protected resource.
   * @access  protected
   * @param  string $requiredScopes Valid scopes.
   * @return mixed                  Data if success.
   *                                Otherwise NULL. 
   * @see self::getValidScopes()
   */
  //To evaluate a given token, 
  //curl "/resource" -d 'access_token=TOKEN'
  public final function resource($requiredScopes = null)
  {
    $request = Request::createFromGlobals();
    if
    (
      !$this->verifyResourceRequest
      (
        $request,
        $this->getResponse(),
        $requiredScopes
      )
    )
    {
      $this->getResponse()->send();
      return null;
    }
    return $this->getAccessTokenData($request);
  }
  /**
   * Uses the configured Grant Types and 
   * provides access token to the client.
   * @access  public
   * @see  http://www.oauth2.com/granttypes
   * @return null
   */
  /*
    curl -u CLIENT_ID:CLIENT_PWD \
         -d "grant_type=client_credentials" \
         "/token?scope=user_create%20user_read"
    
    curl -u CLIENT_ID:CLIENT_PWD \ 
         -d 'grant_type=password&username=USERNAME&password=PASSWORD'
         "/token"
    
    To use the authorized code and acquire a REFRESH_TOKEN, 
    issue a request using 
    curl -u CLIENT_ID:CLIENT_PWD \
         -H "Content-Type: application/x-www-form-urlencoded" \
         -d 'grant_type=authorization_code&code=CODE&redirect_uri=REDIRECT_URI' \
         "/token"
    
    curl -u CLIENT_ID:CLIENT_PWD \
         -H "Content-Type: application/x-www-form-urlencoded" \ 
         -d 'grant_type=refresh_token&refresh_token=REFRESH_TOKEN' 
         "/token"
  */
  public final function token()
  {
    $this->handleTokenRequest(Request::createFromGlobals())->send();
  }
}