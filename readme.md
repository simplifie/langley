# Simplifie Langley

PHP server implementation of Brent Shaffer's OAuth2 Framework.

## Installation

Install it via composer using the following terminal command:

```
composer require simplifie/langley
```


## Usage

Use it as a library for native PHP code or your framework of choice such as CodeIgniter:

```php
use simplifie\OAuth2Server;

require_once 'vendor/autoload.php';
new OAuth2Server();

$config['composer_autoload'] = FCPATH . 'vendor/autoload.php';
$this->load->library('oauth2server');
```

Note: This will also install Brent Shaffer's OAuth2 framework as a requirement.

## Sample cURL Calls

### Authorize Endpoint

To show a UI, issue a GET request using 

```
curl "/authorize&response_type=code&client_id=CLIENT_ID&state=XYZ"
```    

To get an authorization code, issue a request using 

```
curl -d "authorized=yes" "/authorize?response_type=code&client_id=testclient&state=xyz"
```

### Resource Endpoint

To evaluate a given token, issue a request using

```
curl "/resource" -d 'access_token=TOKEN'
```

### Token Endpoint

```
curl -u CLIENT_ID:CLIENT_PWD -d "grant_type=client_credentials" "/token?scope=user_create%20user_read"
```

```
curl -u CLIENT_ID:CLIENT_PWD -d 'grant_type=password&username=USERNAME&password=PASSWORD' "/token"
```
    
To use the authorized code and acquire a REFRESH_TOKEN, issue a request using 
```
curl -u CLIENT_ID:CLIENT_PWD -H "Content-Type: application/x-www-form-urlencoded" -d 'grant_type=authorization_code&code=CODE&redirect_uri=REDIRECT_URI' "/token"
```

```    
curl -u CLIENT_ID:CLIENT_PWD -H "Content-Type: application/x-www-form-urlencoded" -d 'grant_type=refresh_token&refresh_token=REFRESH_TOKEN' "/token"
```

## Contributor

 * [Mark N. Amoin](mailto:prezire@gmail.com)

## Special Thanks
 * [Brent Shaffer's OAuth2 Framework](https://bshaffer.github.io/oauth2-server-php-docs)

## Stay Tuned
 * Langley Framework which, includes DB migration, CRUD for clients (applications), users and scopes.